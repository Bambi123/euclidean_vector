/*
Thomas French
12th of July 2021
Euclidean Vector Implementation

This file is euclidean_vector.cpp file referenced in euclidean_vector.hpp

It contains all details and implementation for all class constructors, destructors,
operatoions, methods, friends and ultilities, as well as a few helping functions for
string analysis.


*/
#include "euclidean_vector.hpp"

// NEW ADDED LIBRARIES
#include <algorithm>
#include <cassert>
#include <memory>
#include <numeric>
#include <sstream>
#include <vector>

// HELPER FUNCTIONS
auto get_vector_length(std::vector<double>::const_iterator start,
                       std::vector<double>::const_iterator end) -> size_t {
	auto size = static_cast<int>(0);
	for (auto i = start; i != end; i++) {
		size++;
	}

	return static_cast<size_t>(size);
}

auto get_size_t_string(size_t const& dim) -> std::string {
	std::stringstream err;
	err << dim;
	std::string err_string;
	err >> err_string;
	return err_string;
}

auto get_dim_err_string(size_t const& dim1, size_t const& dim2) -> std::string {
	return "Dimensions of LHS(" + get_size_t_string(dim1) + ") and RHS(" + get_size_t_string(dim2)
	       + ") do not match";
}

auto multiply(double value, double scalar) -> double {
	return value * scalar;
}

namespace comp6771 {
	// DEFAULT CONSTRUCTOR
	euclidean_vector::euclidean_vector()
	: magnitude_(std::make_unique<double[]>(1))
	, dim_(static_cast<size_t>(1)) {}

	// SINGLE ARGUMENT CONSTRUCTOR
	euclidean_vector::euclidean_vector(int dim)
	: magnitude_(std::make_unique<double[]>(static_cast<size_t>(dim)))
	, dim_(static_cast<size_t>(dim)) {
		for (auto i = 0; i < dim; i++) {
			magnitude_[static_cast<size_t>(i)] = double{0.0};
		}
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [](double elem) { elem = 0.0; });
	}

	// INT DOUBLE CONSTRUCTOR
	euclidean_vector::euclidean_vector(int dim, double value)
	: magnitude_(std::make_unique<double[]>(static_cast<size_t>(dim)))
	, dim_(static_cast<size_t>(dim)) {
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [value](double& elem) {
			elem = value;
		});
	}

	// VECTOR ITERATOR CONSTRUCTOR
	euclidean_vector::euclidean_vector(std::vector<double>::const_iterator start,
	                                   std::vector<double>::const_iterator end) {
		dim_ = get_vector_length(start, end);
		magnitude_ = std::make_unique<double[]>(dim_);
		std::transform(start, end, magnitude_.get(), magnitude_.get(), [](double a, double b) {
			(void)b;
			return a;
		});
	}

	// INITIALIZER lIST CONSTRUCTOR
	euclidean_vector::euclidean_vector(std::initializer_list<double> list) {
		if (list.end() == NULL) {
			dim_ = static_cast<size_t>(0);
		}
		else {
			dim_ = static_cast<size_t>(list.size());
		}

		magnitude_ = std::make_unique<double[]>(dim_);

		std::transform(list.begin(),
		               list.end(),
		               magnitude_.get(),
		               magnitude_.get(),
		               [](double a, double b) {
			               (void)b;
			               return a;
		               });
	}

	// COPY CONSTRUCTOR
	euclidean_vector::euclidean_vector(euclidean_vector const& og) {
		dim_ = og.dim_;
		magnitude_ = std::make_unique<double[]>(dim_);

		std::transform(og.magnitude_.get(),
		               og.magnitude_.get() + dim_,
		               magnitude_.get(),
		               magnitude_.get(),
		               [](double a, double b) {
			               (void)b;
			               return a;
		               });
	}

	// MOVE CONSTRUCTOR
	euclidean_vector::euclidean_vector(euclidean_vector&& og) {
		dim_ = og.dim_;
		og.dim_ = 0;
		magnitude_ = std::move(og.magnitude_);
	}

	// DESTRUCTOR
	euclidean_vector::~euclidean_vector() = default;

	// COPY ASSIGNMENT
	euclidean_vector& euclidean_vector::operator=(euclidean_vector const& og) {
		dim_ = og.dim_;
		magnitude_ = std::make_unique<double[]>(dim_);

		std::transform(og.magnitude_.get(),
		               og.magnitude_.get() + og.dim_,
		               magnitude_.get(),
		               magnitude_.get(),
		               [](double a, double b) {
			               return a;
			               (void)b;
		               });
		return *this;
	}

	// MOVE ASSIGNMENT
	euclidean_vector& euclidean_vector::operator=(euclidean_vector&& og) {
		dim_ = og.dim_;
		og.dim_ = 0;
		magnitude_ = std::move(og.magnitude_);

		return *this;
	}

	// NON-CONST SUBSCRIPT OPERATOR
	double& euclidean_vector::operator[](int index) {
		assert(index >= 0 and static_cast<size_t>(index) < dim_);
		return magnitude_[static_cast<size_t>(index)];
	}

	// CONST SUBSCRIPT OPERATOR
	double euclidean_vector::operator[](int index) const {
		assert(index >= 0 and static_cast<size_t>(index) < dim_);
		return magnitude_[static_cast<size_t>(index)];
	}

	// UNARY PLUS OPERATOR
	euclidean_vector euclidean_vector::operator+() {
		return *this;
	}

	// NEGATION OPERATOR
	euclidean_vector euclidean_vector::operator-() {
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [](double& value) { value *= -1; });
		return *this;
	}

	// COMPOUND ADDITION
	euclidean_vector& euclidean_vector::operator+=(euclidean_vector const& og) {
		if (dim_ != og.dim_) {
			throw std::runtime_error{get_dim_err_string(dim_, og.dim_)};
		}
		std::transform(og.magnitude_.get(),
		               og.magnitude_.get() + dim_,
		               magnitude_.get(),
		               magnitude_.get(),
		               [](double a, double b) { return a + b; });
		return *this;
	}

	// COMPOUND SUBTRACTION
	euclidean_vector& euclidean_vector::operator-=(euclidean_vector const& og) {
		if (dim_ != og.dim_) {
			throw std::runtime_error{get_dim_err_string(dim_, og.dim_)};
		}
		std::transform(og.magnitude_.get(),
		               og.magnitude_.get() + dim_,
		               magnitude_.get(),
		               magnitude_.get(),
		               [](double a, double b) { return a - b; });
		return *this;
	}

	// COMPOUND MULTIPLICATION
	euclidean_vector& euclidean_vector::operator*=(double scalar) {
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [scalar](double& value) {
			value *= scalar;
		});
		return *this;
	}

	// COMPOUND DIVISION
	euclidean_vector& euclidean_vector::operator/=(double scalar) {
		if (scalar == 0) {
			throw std::runtime_error{"Invalid vector division by 0"};
		}
		return (*this) *= (1 / scalar);
	}

	// VECTOR TYPE CONVERSION
	euclidean_vector::operator std::vector<double>() const {
		auto v = std::vector<double>();
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [&v](double value) {
			v.push_back(value);
		});
		return v;
	}

	// LIST TYPE CONVERSION
	euclidean_vector::operator std::list<double>() const {
		auto l = std::list<double>{};
		std::for_each (magnitude_.get(), magnitude_.get() + dim_, [&l](double value) {
			l.push_back(value);
		});
		return l;
	}

	// CONST GET INDEX MEMBER
	double euclidean_vector::at(int index) const {
		if (index < 0 || static_cast<size_t>(index) >= dim_) {
			throw std::runtime_error{"Index " + get_size_t_string(static_cast<size_t>(index))
			                         + " is not valid for this euclidean_vector object"};
		}
		return magnitude_[static_cast<size_t>(index)];
	}

	// NON-CONST GET INDEX MEMBER
	double& euclidean_vector::at(int index) {
		if (index < 0 || static_cast<size_t>(index) >= dim_) {
			throw std::runtime_error{"Index " + get_size_t_string(static_cast<size_t>(index))
			                         + " is not valid for this euclidean_vector object"};
		}
		return magnitude_[static_cast<size_t>(index)];
	}

	// GET DIMENSIONS MEMBER
	int euclidean_vector::dimensions() const {
		return static_cast<int>(dim_);
	}

	// EQUALS FRIEND
	bool operator==(euclidean_vector const& a, euclidean_vector const& b) {
		if (a.dim_ != b.dim_) {
			return false;
		}
		return std::equal(a.magnitude_.get(), a.magnitude_.get() + a.dim_, b.magnitude_.get());
	}

	// NOT EQUALS FRIEND
	bool operator!=(euclidean_vector const& a, euclidean_vector const& b) {
		return !(a == b);
	}

	// ADDITION FRIEND
	euclidean_vector operator+(euclidean_vector const& a, euclidean_vector const& b) {
		if (a.dim_ != b.dim_) {
			throw std::runtime_error{get_dim_err_string(a.dim_, b.dim_)};
		}
		auto new_ev = euclidean_vector(a);

		// Add b to the new euclidean vector
		std::transform(b.magnitude_.get(),
		               b.magnitude_.get() + b.dim_,
		               new_ev.magnitude_.get(),
		               new_ev.magnitude_.get(),
		               [](double b, double val) { return val + b; });
		return new_ev;
	}

	// SUBTRACTION FRIEND
	euclidean_vector operator-(euclidean_vector const& a, euclidean_vector const& b) {
		if (a.dim_ != b.dim_) {
			throw std::runtime_error{get_dim_err_string(a.dim_, b.dim_)};
		}
		auto new_ev = euclidean_vector(a);

		// Subtract b from the new euclidean_vector
		std::transform(b.magnitude_.get(),
		               b.magnitude_.get() + b.dim_,
		               new_ev.magnitude_.get(),
		               new_ev.magnitude_.get(),
		               [](double b, double val) { return val - b; });

		return new_ev;
	}

	// MULTIPLY FRIEND: VECTOR * SCALAR
	euclidean_vector operator*(euclidean_vector const& a, double scalar) {
		return euclidean_vector(a) *= scalar;
	}

	// MULTIPLY FRIEND: SCALAR * VECTOR
	euclidean_vector operator*(double scalar, euclidean_vector const& a) {
		return euclidean_vector(a) *= scalar;
	}

	// DIVISION FRIEND
	euclidean_vector operator/(euclidean_vector const& a, double scalar) {
		return euclidean_vector(a) /= scalar;
	}

	// OUTPUT STREAM FRIEND
	std::ostream& operator<<(std::ostream& os, euclidean_vector const& a) {
		os << "[";
		// Check that the vector is empty so we don't try and get a negative index
		if (a.dim_ <= 0) {
			os << "]";
			return os;
		}
		auto last = a.at(static_cast<int>(a.dim_ - 1));
		std::for_each (a.magnitude_.get(), a.magnitude_.get() + a.dim_, [&os, last](double val) {
			os << val;
			if (val != last) {
				os << " ";
			}
		});
		os << "]";
		return os;
	}

	// EUCLIDEAN NORM UTILITY
	auto euclidean_norm(euclidean_vector const& v) -> double {
		auto result = static_cast<double>(0);
		auto dim = v.dimensions();
		for (auto index = 0; index < dim; index++) {
			auto elem = v.at(index);
			result += elem * elem;
		}

		return sqrt(result);
	}

	// UNIT UTILITY
	auto unit(euclidean_vector const& v) -> euclidean_vector const {
		if (v.dimensions() == 0) {
			throw std::runtime_error{"euclidean_vector with no dimensions does not have a unit "
			                         "vector"};
		}

		auto new_ev = euclidean_vector(v);
		auto norm = euclidean_norm(v);
		auto dim = v.dimensions();

		if (norm == 0) {
			throw std::runtime_error{"euclidean_vector with zero euclidean normal does not have a "
			                         "unit vector"};
		}

		for (auto index = 0; index < dim; index++) {
			new_ev.at(index) /= norm;
		}
		return new_ev;
	}

	// DOT UTILITY
	auto dot(euclidean_vector const& a, euclidean_vector const& b) -> double {
		auto a_dim = a.dimensions();
		auto b_dim = b.dimensions();
		if (a_dim != b_dim) {
			throw std::runtime_error{
			   get_dim_err_string(static_cast<size_t>(a_dim), static_cast<size_t>(b_dim))};
		}

		auto result = static_cast<double>(0);
		for (auto index = 0; index < a_dim; index++) {
			result += a.at(index) * b.at(index);
		}
		return result;
	}

} // namespace comp6771
