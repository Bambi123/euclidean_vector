/*
Thomas French
z5206283
Due Date: 12th of July, 2021

In physics and mathematics, a euclidean vector is a comprehensive
element of the vector space.  Such an element contains a number
of dimensions and a magnitude of unit strenght in each dimension.
Historically, euclidean vectors were geometric properties before
they were ever used in physics.

The following library outlines the implementation for all operations
for a euclidean vector c++ object.  All written implementation can
be found in euclidean_vector.cpp.
*/

#include <list>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace comp6771 {
	class euclidean_vector_error : public std::runtime_error {
	public:
		explicit euclidean_vector_error(std::string const& what)
		: std::runtime_error(what) {}
	};

	class euclidean_vector {
	public:
		// CONSTRUCTORS
		[[noexcept]] euclidean_vector(); // DEFAULT CONSTRUCTOR
		[[noexcept]] explicit euclidean_vector(int); // SINGLE ARGUMENT CONSTRUCTOR
		[[noexcept]] euclidean_vector(int, double); // CONSTRUCTOR
		[[noexcept]] euclidean_vector(std::vector<double>::const_iterator,
		                              std::vector<double>::const_iterator); // CONSTRUCTOR
		[[noexcept]] euclidean_vector(std::initializer_list<double>); // CONSTRUCTOR
		[[noexcept]] euclidean_vector(euclidean_vector const&); // COPY CONSTRUCTOR
		[[noexcept]] euclidean_vector(euclidean_vector&&); // MOVE CONSTRUCTOR
		[[noexcept]] ~euclidean_vector(); // DESTRUCTOR

		// OPERATIONS
		[[noexcept]] euclidean_vector& operator=(euclidean_vector const&); // COPY ASSIGNMENT
		[[noexcept]] euclidean_vector& operator=(euclidean_vector&&); // MOVE ASSIGNMENT
		[[noexcept]] double& operator[](int); // NON-CONST SUBSCRIPT OPERATOR
		[[noexcept]] double operator[](int) const; // CONST SUBSCRIPT OPERATOR
		[[noexcept]] euclidean_vector operator+(); // UNARYPLUS OPERATOR
		[[noexcept]] euclidean_vector operator-(); // NEGATION OPERATOR
		euclidean_vector& operator+=(euclidean_vector const&); // COMPOUND ADDITION OPERATOR
		euclidean_vector& operator-=(euclidean_vector const&); // COMPOUND SUBTRACTION OPERATOR
		[[noexcept]] euclidean_vector& operator*=(double); // COMPOUND MULTIPLICATION OPERATOR
		euclidean_vector& operator/=(double); // COMPOUND DIVISION OPERATOR
		[[noexcept]] explicit operator std::vector<double>() const; // CONST VECTOR TYPE CONVERSION
		[[noexcept]] explicit operator std::list<double>() const; // CONST LIST TYPE CONVERSION

		// MEMBER FUNCTIONS
		double at(int) const; // CONST GET INDEX METHOD
		double& at(int); // NON CONST GET INDEX METHOD
		[[noexcept]] int dimensions() const; // GET DIMENSIONS METHOD

		// FRIEND FUNCTIONS
		friend bool operator==(euclidean_vector const&, euclidean_vector const&); // EQUAL FRIEND
		friend bool operator!=(euclidean_vector const&, euclidean_vector const&); // NOT EQUAL FRIEND
		friend euclidean_vector operator+(euclidean_vector const&, euclidean_vector const&); // ADDITION
		                                                                                     // FRIEND
		friend euclidean_vector
		operator-(euclidean_vector const&, euclidean_vector const&); // SUBTRACTION
		                                                             // FRIEND
		friend euclidean_vector operator*(euclidean_vector const&, double); // MULTIPLY FRIEND VECTOR
		                                                                    // * SCALAR
		friend euclidean_vector operator*(double, euclidean_vector const&); // MULTIPLY FRIEND SCALAR
		                                                                    // * VECTOR
		friend euclidean_vector operator/(euclidean_vector const&, double); // DIVIDE FRIEND
		friend std::ostream& operator<<(std::ostream&, euclidean_vector const&); // OUTPUT STREAM
		                                                                         // FRIEND

	private:
		// ass2 spec requires we use double[]
		// NOLINTNEXTLINE(modernize-avoid-c-arrays)
		std::unique_ptr<double[]> magnitude_;
		size_t dim_;
		// TODO more if needed
	};

	// UTILITY FUNCTIONS
	[[noexcept]] auto euclidean_norm(euclidean_vector const&) -> double; // EUCLIDEAN NORM UTILITY
	auto unit(euclidean_vector const&) -> euclidean_vector const; // UNIT UTILITY
	auto dot(euclidean_vector const&, euclidean_vector const&) -> double; // DOT UTILITY

} // namespace comp6771
#endif // COMP6771_EUCLIDEAN_VECTOR_HPP
