/*
Thomas French z5206283
12th of July, 2021
euclidean_vector_test_constructor.cpp

The contents of this file detail the testing implementation of all
the constructor functions contained in euclidean_vector.hpp

Testing is separated into test cases based on the literal function definition in
the class, one test case per constructor.  Within these test cases, sections are created
per different edge case scenario for each constructor, including non-const and const
definitions, empty vectors, and expected behaviour of each constructor.
*/

#include "comp6771/euclidean_vector.hpp"

#include <catch2/catch.hpp>

TEST_CASE("Default Constructor") {
	SECTION("Const Default Constructor") {
		auto const a = comp6771::euclidean_vector();
		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);
	}

	SECTION("Non-Const Default Constructor") {
		auto a = comp6771::euclidean_vector();
		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);
	}
}

TEST_CASE("Single Argument Constructor") {
	SECTION("Const Single Argument Constructor") {
		auto const a = comp6771::euclidean_vector(3);
		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Single Argument Constructor") {
		auto a = comp6771::euclidean_vector(3);
		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Zero Dimension Single Argument Constructor") {
		auto a = comp6771::euclidean_vector(0);
		REQUIRE(a.dimensions() == 0);
	}

	SECTION("Copy Integer Single Argument Constructor") {
		auto i = int{3};
		auto a = comp6771::euclidean_vector(i);
		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Large Single Argument Constructor") {
		auto a = comp6771::euclidean_vector(99999);
		REQUIRE(a.dimensions() == 99999);
	}
}

TEST_CASE("Int Double Constructor") {
	SECTION("Const Int Double Constructor") {
		auto const a = comp6771::euclidean_vector(3, 2.5);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Int Double Constructor") {
		auto a = comp6771::euclidean_vector(3, 2.5);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Zero Dimension Int Double Constructor") {
		auto a = comp6771::euclidean_vector(0, 2.5);
		REQUIRE(a.dimensions() == 0);
	}

	SECTION("Negative Values Int Double Constructor") {
		auto a = comp6771::euclidean_vector(3, -2.5);
		CHECK(abs(a[0] + 2.5) < 0.0001);
		CHECK(abs(a[1] + 2.5) < 0.0001);
		CHECK(abs(a[2] + 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Large Dimension Int Double Constructor") {
		auto a = comp6771::euclidean_vector(99999, 2.5);
		REQUIRE(a.dimensions() == 99999);
	}

	SECTION("Copy Value Int Double Constructor") {
		auto val = double{2.5};
		auto a = comp6771::euclidean_vector(3, val);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Copy Dimension Int Double Constructor") {
		auto dim = int{3};
		auto a = comp6771::euclidean_vector(dim, 2.5);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Copy Both Dimension and Value Int Double Constructor") {
		auto dim = int{3};
		auto val = double{2.5};
		auto a = comp6771::euclidean_vector(dim, val);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}
}

TEST_CASE("Vector Iterator Constructor") {
	SECTION("Const Vector Iterator Constructor") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Vector Iterator Constructor") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Given Vector is Const Vector Iterator Constructor") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Given Vector is Non-Const Vector Iterator Constructor") {
		auto v = std::vector<double>{1.1, 2.2, 3.3};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Given Vector is Empty Vector Iterator Constructor") {
		auto v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		REQUIRE(a.dimensions() == 0);
	}
}

TEST_CASE("Iniatilizer List Constructor") {
	SECTION("Const Initlalizer List Construction") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Initlalizer List Construction") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Size 1 Initlalizer List Construction") {
		auto a = comp6771::euclidean_vector{1.1};
		CHECK(abs(a[0] - 1.1) < 0.0001);
		REQUIRE(a.dimensions() == 1);
	}

	SECTION("Empty Initlalizer List Construction") {
		auto a = comp6771::euclidean_vector{};
		REQUIRE(a.dimensions() == 1);
	}
}

TEST_CASE("Copy Constructor") {
	SECTION("Const Copy Constructor") {
		auto const a = comp6771::euclidean_vector(3, 2.5);
		auto const b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Non-Const Copy Constructor") {
		auto const a = comp6771::euclidean_vector(3, 2.5);
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Copy Non-Const Non-Const Copy Constructor") {
		auto a = comp6771::euclidean_vector(3, 2.5);
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Copy Non-Const Const Copy Constructor") {
		auto a = comp6771::euclidean_vector(3, 2.5);
		auto const b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Default Constructor Copy Constructor") {
		auto a = comp6771::euclidean_vector();
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		CHECK(abs(b[0]) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("Single Argument Constructor Copy Constructor") {
		auto a = comp6771::euclidean_vector(3);
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0]) < 0.0001);
		CHECK(abs(b[1]) < 0.0001);
		CHECK(abs(b[2]) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector(0);
		auto d = comp6771::euclidean_vector(c);

		REQUIRE(c.dimensions() == 0);
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Int Double Constructor Copy Constructor") {
		auto a = comp6771::euclidean_vector(3, 2.5);
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector(0, 2.5);
		auto d = comp6771::euclidean_vector(c);

		REQUIRE(c.dimensions() == 0);
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Vector Constructor Copy Constructor") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const w = std::vector<double>{};
		auto c = comp6771::euclidean_vector(w.begin(), w.end());
		auto d = comp6771::euclidean_vector(c);

		REQUIRE(c.dimensions() == 0);
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Initializer List Constructor Copy Constructor") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector{};
		auto d = comp6771::euclidean_vector(c);

		REQUIRE(c.dimensions() == 1);
		REQUIRE(d.dimensions() == 1);
	}
}

TEST_CASE("Move Constructor") {
	SECTION("Const Move Constructor") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Non-Const Move Constructor") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Const Original Non-Const Move Constructor") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(std::move(a));

		// Const 'a' calls move as copy constructor
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		REQUIRE(a[0] == 1.1); // Const qualified 'a' should hold memory address
	}

	SECTION("Const Original Const Move Constructor") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(std::move(a));

		// Const 'a' calls move as copy constructor
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		REQUIRE(a[0] == 1.1); // Const qualified 'a' should hold memory address
	}

	SECTION("Default Constructor Move Constructor") {
		auto a = comp6771::euclidean_vector();
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0]) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("Single Argument Constructor Move Constructor") {
		auto a = comp6771::euclidean_vector(3);
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0]) < 0.0001);
		CHECK(abs(b[1]) < 0.0001);
		CHECK(abs(b[2]) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector(0);
		auto d = comp6771::euclidean_vector(std::move(c));
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Int Double Constructor Move Constructor") {
		auto a = comp6771::euclidean_vector(3, 2.5);
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);
		CHECK(abs(b[0] - 2.5) < 0.0001);
		CHECK(abs(b[1] - 2.5) < 0.0001);
		CHECK(abs(b[2] - 2.5) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector(0, 2.5);
		auto d = comp6771::euclidean_vector(std::move(c));
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Vector Constructor Move Constructor") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);
		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const w = std::vector<double>{};
		auto c = comp6771::euclidean_vector(w.begin(), w.end());
		auto d = comp6771::euclidean_vector(std::move(c));
		REQUIRE(d.dimensions() == 0);
	}

	SECTION("Initializer List Constructor Move Constructor") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(std::move(a));

		REQUIRE(a.dimensions() == 0);
		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto c = comp6771::euclidean_vector{};
		auto d = comp6771::euclidean_vector(std::move(c));
		REQUIRE(d.dimensions() == 1);
	}
}

TEST_CASE("Destructor") {
	SECTION("Default Constructor Destruction") {
		auto const a = comp6771::euclidean_vector();
		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		a.~euclidean_vector();
	}

	SECTION("Single Argument Constructor Destruction") {
		auto const a = comp6771::euclidean_vector(3);
		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a.~euclidean_vector();
	}

	SECTION("Int Double Constructor Destruction") {
		auto const a = comp6771::euclidean_vector(3, 2.5);
		CHECK(abs(a[0] - 2.5) < 0.0001);
		CHECK(abs(a[1] - 2.5) < 0.0001);
		CHECK(abs(a[2] - 2.5) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a.~euclidean_vector();
	}

	SECTION("Vector Constructor Destruction") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());
		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a.~euclidean_vector();
	}

	SECTION("Initializer List Constructor Destruction") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a.~euclidean_vector();
	}
}
