/*
Thomas French z5206283
12th of July, 2021
euclidean_vector_test_friends.cpp

The contents of this file detail the testing implementation of all
the friends functions contained in euclidean_vector.hpp

Testing is separated into test cases based on the literal function definition in
the class, one test case per friend function.  Within these test cases, sections are created
per different edge case scenario for each function including non-const and const
definitions, empty vectors, and expected behaviour of each function.
*/

#include "comp6771/euclidean_vector.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE("Equals Friend") {
	SECTION("Const Const Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK((a == b));
	}

	SECTION("Non-Const Const Equals Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK((a == b));
	}

	SECTION("Non-Const Non-Const Equals Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK((a == b));
	}

	SECTION("Const Non-Const Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK((a == b));
	}

	SECTION("Mismatching Dimensions Empty Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};
		CHECK(!(a == b));
	}

	SECTION("LHS Empty Equals Friend") {
		auto const v = std::vector<double>{};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{4.4};
		CHECK(!(a == b));
	}

	SECTION("RHS Empty Equals Friend") {
		auto const v = std::vector<double>{};
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(v.begin(), v.end());
		CHECK(!(a == b));
	}

	SECTION("LHS and RHS Empty Equals Friend") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());
		CHECK((a == b));
	}
}

TEST_CASE("Not Equals Friend") {
	SECTION("Const Const Not Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK(!(a != b));
	}

	SECTION("Non-Const Const Not Equals Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK(!(a != b));
	}

	SECTION("Non-Const Non-Const Not Equals Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK(!(a != b));
	}

	SECTION("Const Non-Const Not Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		CHECK(!(a != b));
	}

	SECTION("Mismatching Dimensions Empty Not Equals Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};
		CHECK((a != b));
	}

	SECTION("LHS Empty Not Equals Friend") {
		auto const v = std::vector<double>{};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{4.4};
		CHECK((a != b));
	}

	SECTION("RHS Empty Not Equals Friend") {
		auto const v = std::vector<double>{};
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(v.begin(), v.end());
		CHECK((a != b));
	}

	SECTION("LHS and RHS Empty Not Equals Friend") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());
		CHECK(!(a != b));
	}
}

TEST_CASE("Addition Friend") {
	SECTION("Const Const Addition Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a + b;

		CHECK(abs(c[0] - 5.5) < 0.0001);
		CHECK(abs(c[1] - 7.7) < 0.0001);
		CHECK(abs(c[2] - 9.9) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Non-Const Const Addition Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a + b;

		CHECK(abs(c[0] - 5.5) < 0.0001);
		CHECK(abs(c[1] - 7.7) < 0.0001);
		CHECK(abs(c[2] - 9.9) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Non-Const Non-Const Addition Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a + b;

		CHECK(abs(c[0] - 5.5) < 0.0001);
		CHECK(abs(c[1] - 7.7) < 0.0001);
		CHECK(abs(c[2] - 9.9) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Const Non-Const Addition Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a + b;

		CHECK(abs(c[0] - 5.5) < 0.0001);
		CHECK(abs(c[1] - 7.7) < 0.0001);
		CHECK(abs(c[2] - 9.9) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Mismatching Dimensions Addition Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		CHECK_THROWS(a + b);
	}

	SECTION("LHS Empty Addition Friend") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		CHECK_THROWS(a + b);

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("RHS Empty Addition Friend") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);

		CHECK_THROWS(a + b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);
	}

	SECTION("LHS and RHS Empty Addition Friend") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);

		a + b;

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Subtraction Friend") {
	SECTION("Const Const Subtraction Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{-1.1, -2.2, -3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] + 1.1) < 0.0001);
		CHECK(abs(b[1] + 2.2) < 0.0001);
		CHECK(abs(b[2] + 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a - b;

		CHECK(abs(c[0] - 2.2) < 0.0001);
		CHECK(abs(c[1] - 4.4) < 0.0001);
		CHECK(abs(c[2] - 6.6) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Non-Const Const Subtraction Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{-1.1, -2.2, -3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] + 1.1) < 0.0001);
		CHECK(abs(b[1] + 2.2) < 0.0001);
		CHECK(abs(b[2] + 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a - b;

		CHECK(abs(c[0] - 2.2) < 0.0001);
		CHECK(abs(c[1] - 4.4) < 0.0001);
		CHECK(abs(c[2] - 6.6) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Non-Const Non-Const Subtraction Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{-1.1, -2.2, -3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] + 1.1) < 0.0001);
		CHECK(abs(b[1] + 2.2) < 0.0001);
		CHECK(abs(b[2] + 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a - b;

		CHECK(abs(c[0] - 2.2) < 0.0001);
		CHECK(abs(c[1] - 4.4) < 0.0001);
		CHECK(abs(c[2] - 6.6) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Const Non-Const Subtraction Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{-1.1, -2.2, -3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] + 1.1) < 0.0001);
		CHECK(abs(b[1] + 2.2) < 0.0001);
		CHECK(abs(b[2] + 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		auto const c = a - b;

		CHECK(abs(c[0] - 2.2) < 0.0001);
		CHECK(abs(c[1] - 4.4) < 0.0001);
		CHECK(abs(c[2] - 6.6) < 0.0001);
		REQUIRE(c.dimensions() == 3);
	}

	SECTION("Mismatching Dimensions Subtraction Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		CHECK_THROWS(a - b);
	}

	SECTION("LHS Empty Subtraction Friend") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		CHECK_THROWS(a - b);

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("RHS Empty Subtraction Friend") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);

		CHECK_THROWS(a - b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);
	}

	SECTION("LHS and RHS Empty Subtraction Friend") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);

		a - b;

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Multiply Friend") {
	SECTION("Const Multiply Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto b = a * 2;

		CHECK(abs(b[0] - 2.2) < 0.0001);
		CHECK(abs(b[1] - 4.4) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Non-Const Multiply Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto b = a * 2;

		CHECK(abs(b[0] - 2.2) < 0.0001);
		CHECK(abs(b[1] - 4.4) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Vector by Scalar Multiply Friend") {
		auto a = comp6771::euclidean_vector{1.1};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		auto b = a * 2;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		CHECK(abs(b[0] - 2.2) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("Scalar by Vector Multiply Friend") {
		auto a = comp6771::euclidean_vector{1.1};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		auto b = 2 * a;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		CHECK(abs(b[0] - 2.2) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("Scalar is Zero Multiply Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto b = a * 0;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0]) < 0.0001);
		CHECK(abs(b[1]) < 0.0001);
		CHECK(abs(b[2]) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}
}

TEST_CASE("Division Friend") {
	SECTION("Const Division Friend") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto b = a / 0.5;

		CHECK(abs(b[0] - 2.2) < 0.0001);
		CHECK(abs(b[1] - 4.4) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	};

	SECTION("Non-Const Division Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto b = a / 0.5;

		CHECK(abs(b[0] - 2.2) < 0.0001);
		CHECK(abs(b[1] - 4.4) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	};

	SECTION("Scalar is Zero Division Friend") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK_THROWS(a / 0);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	};
}

TEST_CASE("Output Stream Friend") {
	SECTION("Const Output Stream") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		auto oss = std::ostringstream{};
		oss << a;
		CHECK(oss.str() == "[1.1 2.2 3.3]");
	}

	SECTION("Non-Const Output Stream") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		auto oss = std::ostringstream{};
		oss << a;
		CHECK(oss.str() == "[1.1 2.2 3.3]");
	}

	SECTION("Single Element Vector Output Stream") {
		auto a = comp6771::euclidean_vector{1.1};

		auto oss = std::ostringstream{};
		oss << a;
		CHECK(oss.str() == "[1.1]");
	}

	SECTION("Empty Vector Output Stream") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		auto oss = std::ostringstream{};
		oss << a;
		CHECK(oss.str() == "[]");
	}
}
