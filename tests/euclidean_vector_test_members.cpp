/*
Thomas French z5206283
12th of July, 2021
euclidean_vector_test_members.cpp

The contents of this file detail the testing implementation of all
the friends functions contained in euclidean_vector.hpp

Testing is separated into test cases based on the literal function definition in
the class, one test case per member function.  Within these test cases, sections are created
per different edge case scenario for each function including non-const and const
definitions, empty vectors, and expected behaviour of each function.
*/

#include "comp6771/euclidean_vector.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE("Const: Get Index Member") {
	SECTION("Const Euclidean Vector Const Index Member") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 2.2) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
	}

	SECTION("Empty Euclidean Vector Const Index Member") {
		auto v = std::vector<double>{};
		auto const a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK_THROWS(a.at(0));
		REQUIRE(a.dimensions() == 0);
	}

	SECTION("Invalid Index Upper Bound Const Index Member") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK_THROWS(a.at(3));
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Invalid Index Lower Bound Const Index Member") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK_THROWS(a.at(-1));
		REQUIRE(a.dimensions() == 3);
	}
}

TEST_CASE("Non-const: Get Index Member") {
	SECTION("Const Euclidean Vector Non-Const Index Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 2.2) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
	}

	SECTION("Empty Euclidean Vector Non-Const Index Member") {
		auto v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK_THROWS(a.at(0));
		REQUIRE(a.dimensions() == 0);
	}

	SECTION("Invalid Index Upper Bound Non-Const Index Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK_THROWS(a.at(3));
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Invalid Index Lower Bound Non-Const Index Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK_THROWS(a.at(-1));
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Manipulating Object Non-Const Index Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 2.2) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a.at(1) = 4.4;

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 4.4) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Manipulating Out of Bounds Index Non-Const Index Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 2.2) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK_THROWS(a.at(3) = 4.4);
		CHECK_THROWS(a.at(-1) = 4.4);

		CHECK(abs(a.at(0) - 1.1) < 0.0001);
		CHECK(abs(a.at(1) - 2.2) < 0.0001);
		CHECK(abs(a.at(2) - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}
}

TEST_CASE("Get Dimensions Member") {
	SECTION("Const Get Dimensions Member") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(a.dimensions() == 3);
	}

	SECTION("Non Const Get Dimensions Member") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(a.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Get Dimensions Member") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(a.dimensions() == 0);
	}
}
