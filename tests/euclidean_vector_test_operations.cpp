/*
Thomas French z5206283
12th of July, 2021
euclidean_vector_test_operations.cpp

The contents of this file detail the testing implementation of all
the operator functions contained in euclidean_vector.hpp

Testing is separated into test cases based on the literal function definition in
the class, one test case per operator function.  Within these test cases, sections are created
per different edge case scenario for each function including non-const and const
definitions, empty vectors, and expected behaviour of each function.
*/

#include "comp6771/euclidean_vector.hpp"

#include <catch2/catch.hpp>

TEST_CASE("Copy Assignment Operation Overload") {
	SECTION("Const Copy Assignment") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});
		auto b = comp6771::euclidean_vector({4.4});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = a;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Non-Const Copy Assignment") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = a;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Copy Assignment") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{4.4};

		REQUIRE(a.dimensions() == 0);
		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = a;

		REQUIRE(a.dimensions() == 0);
		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Move Assignment Operation Overload") {
	SECTION("Const Move Assignment") {
		auto const a = comp6771::euclidean_vector({1.1, 2.2, 3.3});
		auto b = comp6771::euclidean_vector({4.4});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = std::move(a);

		// Since A is const, it should still be pointing at the address
		// despite it being moved
		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Non-Const Move Assignment") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});
		auto b = comp6771::euclidean_vector({4.4});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = std::move(a);

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Move Assignment") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector({4.4});

		REQUIRE(a.dimensions() == 0);
		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		b = std::move(a);

		REQUIRE(a.dimensions() == 0);
		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Subscript Operation Overload") {
	SECTION("Const Euclidean Vector Subscript") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Euclidean Vector Subscript") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a[0] = 4.4;

		CHECK(abs(a[0] - 4.4) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Default Constructor Subscript") {
		auto a = comp6771::euclidean_vector();

		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		a[0] = 9.9;

		CHECK(abs(a[0] - 9.9) < 0.0001);
		REQUIRE(a.dimensions() == 1);
	}
}

TEST_CASE("Unary Plus Operation Overload") {
	SECTION("Non-Const Euclidean Vector Unary Plus") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		+a;

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Unary Plus") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		REQUIRE(a.dimensions() == 0);

		+a;

		REQUIRE(a.dimensions() == 0);
	}
}

TEST_CASE("Negation Operation Overload") {
	SECTION("Non-Const Euclidean Vector Negation") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});
		auto b = comp6771::euclidean_vector({-1.1, -2.2, -3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] + 1.1) < 0.0001);
		CHECK(abs(b[1] + 2.2) < 0.0001);
		CHECK(abs(b[2] + 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		-b;

		CHECK(abs(a[0] - b[0]) < 0.0001);
		CHECK(abs(a[1] - b[1]) < 0.0001);
		CHECK(abs(a[2] - b[2]) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Negation") {
		auto v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(a.dimensions() == 0);

		-a;

		CHECK(a.dimensions() == 0);
	}
}

TEST_CASE("Compound Addition Operation Overload") {
	SECTION("Const Compound Addition") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		a += b;

		CHECK(abs(a[0] - 5.5) < 0.0001);
		CHECK(abs(a[1] - 7.7) < 0.0001);
		CHECK(abs(a[2] - 9.9) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Non-Const Compound Addition") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4, 5.5, 6.6};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		a += b;

		CHECK(abs(a[0] - 5.5) < 0.0001);
		CHECK(abs(a[1] - 7.7) < 0.0001);
		CHECK(abs(a[2] - 9.9) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Incorrect Dimensions Compound Addition") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		CHECK_THROWS(a += b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("LHS Empty Compound Addition") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{4.4, 5.5, 6.6};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		CHECK_THROWS(a += b);

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("RHS Empty Compound Addition") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto const w = std::vector<double>{};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);

		CHECK_THROWS(a += b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);
	}

	SECTION("LHS and RHS Empty Compound Addition") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);

		a += b;

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Compound Subtraction Operation Overload") {
	SECTION("Const Compound Subtraction") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});
		auto const b = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		a -= b;

		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
	}

	SECTION("Non-Const Compound Subtraction") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{1.1, 2.2, 3.3};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 1.1) < 0.0001);
		CHECK(abs(b[1] - 2.2) < 0.0001);
		CHECK(abs(b[2] - 3.3) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		a -= b;

		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
	}

	SECTION("Incorrect Dimensions Compound Subtraction") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto b = comp6771::euclidean_vector{4.4};

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);

		CHECK_THROWS(a -= b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("LHS Empty Compound Subtraction") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{4.4, 5.5, 6.6};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);

		CHECK_THROWS(a -= b);

		REQUIRE(a.dimensions() == 0);

		CHECK(abs(b[0] - 4.4) < 0.0001);
		CHECK(abs(b[1] - 5.5) < 0.0001);
		CHECK(abs(b[2] - 6.6) < 0.0001);
		REQUIRE(b.dimensions() == 3);
	}

	SECTION("RHS is Empty Compound Subtraction") {
		auto const v = std::vector<double>{1.1, 2.2, 3.3};
		auto const w = std::vector<double>{};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);

		CHECK_THROWS(a -= b);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		REQUIRE(b.dimensions() == 0);
	}

	SECTION("LHS and RHS is Empty Compound Subtraction") {
		auto const v = std::vector<double>{};
		auto const w = std::vector<double>{};

		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);

		a -= b;

		REQUIRE(a.dimensions() == 0);

		REQUIRE(b.dimensions() == 0);
	}
}

TEST_CASE("Compound Multiplication Operation Overload") {
	SECTION("Non-Const Compound Multiplication") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a *= 2.5;

		CHECK(abs(a[0] - 2.75) < 0.0001);
		CHECK(abs(a[1] - 5.5) < 0.0001);
		CHECK(abs(a[2] - 8.25) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Scalar is Zero Compound Multiplication") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a *= 0.0;

		CHECK(abs(a[0]) < 0.0001);
		CHECK(abs(a[1]) < 0.0001);
		CHECK(abs(a[2]) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Compound Multiplication") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(a.dimensions() == 0);

		a *= 2.5;

		CHECK(a.dimensions() == 0);
	}
}

TEST_CASE("Compound Division Operation Overload") {
	SECTION("Non-Const Compound Division") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		a /= 0.5;

		CHECK(abs(a[0] - 2.2) < 0.0001);
		CHECK(abs(a[1] - 4.4) < 0.0001);
		CHECK(abs(a[2] - 6.6) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Scalar is Zero Compound Division") {
		auto a = comp6771::euclidean_vector({1.1, 2.2, 3.3});

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		CHECK_THROWS(a /= 0.0);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);
	}

	SECTION("Empty Euclidean Vector Compound Division") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK(a.dimensions() == 0);

		a /= 2.5;

		CHECK(a.dimensions() == 0);
	}
}

TEST_CASE("Vector Type Conversion Operation Overload") {
	SECTION("Const Vector Type Conversion") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const v = static_cast<std::vector<double>>(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto it = v.begin();
		CHECK(abs(*it - 1.1) < 0.0001);
		it++;
		CHECK(abs(*it - 2.2) < 0.0001);
		it++;
		CHECK(abs(*it - 3.3) < 0.0001);
		REQUIRE(v.size() == 3);
	}

	SECTION("Non-Const Vector Type Conversion") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const v = static_cast<std::vector<double>>(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto it = v.begin();
		CHECK(abs(*it - 1.1) < 0.0001);
		it++;
		CHECK(abs(*it - 2.2) < 0.0001);
		it++;
		CHECK(abs(*it - 3.3) < 0.0001);
		REQUIRE(v.size() == 3);
	}

	SECTION("Empty Euclidean Vector Type Conversion") {
		auto const w = std::vector<double>{};
		auto a = comp6771::euclidean_vector(w.begin(), w.end());

		auto const v = static_cast<std::vector<double>>(a);

		REQUIRE(a.dimensions() == 0);

		REQUIRE(v.size() == 0);
	}

	SECTION("Single Element Vector Type Conversion") {
		auto a = comp6771::euclidean_vector();
		auto const v = static_cast<std::vector<double>>(a);

		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		auto it = v.begin();
		CHECK(abs(*it) < 0.00001);
		REQUIRE(v.size() == 1);
	}
}

TEST_CASE("List Type Conversion Operation Overload") {
	SECTION("Const List Type Conversion") {
		auto const a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const l = static_cast<std::list<double>>(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto it = l.begin();
		CHECK(abs(*it - 1.1) < 0.0001);
		it++;
		CHECK(abs(*it - 2.2) < 0.0001);
		it++;
		CHECK(abs(*it - 3.3) < 0.0001);
		REQUIRE(l.size() == 3);
	}

	SECTION("Non-Const List Type Conversion") {
		auto a = comp6771::euclidean_vector{1.1, 2.2, 3.3};
		auto const l = static_cast<std::list<double>>(a);

		CHECK(abs(a[0] - 1.1) < 0.0001);
		CHECK(abs(a[1] - 2.2) < 0.0001);
		CHECK(abs(a[2] - 3.3) < 0.0001);
		REQUIRE(a.dimensions() == 3);

		auto it = l.begin();
		CHECK(abs(*it - 1.1) < 0.0001);
		it++;
		CHECK(abs(*it - 2.2) < 0.0001);
		it++;
		CHECK(abs(*it - 3.3) < 0.0001);
		REQUIRE(l.size() == 3);
	}

	SECTION("Empty List Type Conversion") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto const l = static_cast<std::list<double>>(a);

		REQUIRE(a.dimensions() == 0);

		REQUIRE(l.size() == 0);
	}

	SECTION("Single Element List Type Conversion") {
		auto a = comp6771::euclidean_vector();
		auto const l = static_cast<std::list<double>>(a);

		CHECK(abs(a[0]) < 0.0001);
		REQUIRE(a.dimensions() == 1);

		auto it = l.begin();
		CHECK(abs(*it) < 0.0001);
		REQUIRE(l.size() == 1);
	}
}
