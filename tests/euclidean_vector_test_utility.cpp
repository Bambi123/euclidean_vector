/*
Thomas French z5206283
12th of July, 2021
euclidean_vector_test_utility.cpp

The contents of this file detail the testing implementation of all
the utility functions contained in euclidean_vector.hpp

Testing is separated into test cases based on the literal function definition in
the class, one test case per utility function.  Within these test cases, sections are created
per different edge case scenario for each function including non-const and const
definitions, empty vectors, and expected behaviour of each function.
*/

#include "comp6771/euclidean_vector.hpp"

#include <catch2/catch.hpp>

TEST_CASE("Euclidean Norm Utility") {
	SECTION("Const Norm Utility") {
		auto const a = comp6771::euclidean_vector{1, 2, 3};
		CHECK(abs(euclidean_norm(a) - sqrt(1 * 1 + 2 * 2 + 3 * 3)) < 0.0001);
	}

	SECTION("Non-Const Norm Utility") {
		auto a = comp6771::euclidean_vector{1, 2, 3};
		CHECK(abs(euclidean_norm(a) - sqrt(1 * 1 + 2 * 2 + 3 * 3)) < 0.0001);
	}

	SECTION("Single Element Vector Norm Utility") {
		auto a = comp6771::euclidean_vector{3};
		CHECK(abs(euclidean_norm(a) - 3) < 0.0001);
	}

	SECTION("Empty Vector Norm Utility") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		CHECK(abs(euclidean_norm(a)) < 0.0001);
	}

	SECTION("Zero Euclidean Norm Unit Utility") {
		auto a = comp6771::euclidean_vector(5);
		CHECK(abs(euclidean_norm(a)) < 0.0001);
	}
}

TEST_CASE("Unit Utility") {
	SECTION("Const Unit Utility") {
		auto const a = comp6771::euclidean_vector{1, 2, 3, 4};

		auto const b = unit(a);

		CHECK(abs(b[0] - 1 / sqrt(30)) < 0.0001);
		CHECK(abs(b[1] - 2 / sqrt(30)) < 0.0001);
		CHECK(abs(b[2] - 3 / sqrt(30)) < 0.0001);
		CHECK(abs(b[3] - 4 / sqrt(30)) < 0.0001);
		REQUIRE(b.dimensions() == 4);
	}

	SECTION("Non-Const Unit Utility") {
		auto a = comp6771::euclidean_vector{1, 2, 3, 4};

		auto const b = unit(a);

		CHECK(abs(b[0] - 1 / sqrt(30)) < 0.0001);
		CHECK(abs(b[1] - 2 / sqrt(30)) < 0.0001);
		CHECK(abs(b[2] - 3 / sqrt(30)) < 0.0001);
		CHECK(abs(b[3] - 4 / sqrt(30)) < 0.0001);
		REQUIRE(b.dimensions() == 4);
	}

	SECTION("Single Element Unit Utility") {
		auto a = comp6771::euclidean_vector{3};

		auto const b = unit(a);

		CHECK(abs(b[0] - 1) < 0.0001);
		REQUIRE(b.dimensions() == 1);
	}

	SECTION("Empty Vector Unit Utility") {
		auto const v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK_THROWS(unit(a));
	}

	SECTION("Zero Euclidean Norm Unit Utility") {
		auto a = comp6771::euclidean_vector(5);
		CHECK(abs(euclidean_norm(a)) < 0.0001);
		CHECK_THROWS(unit(a));
	}
}

TEST_CASE("Dot Utility") {
	SECTION("Const Const Dot Utility") {
		auto const a = comp6771::euclidean_vector{1, 2, 3};
		auto const b = comp6771::euclidean_vector{1, 2, 3};

		CHECK(abs(dot(a, b) - 14) < 0.0001);
	}

	SECTION("Non-Const Const Dot Utility") {
		auto a = comp6771::euclidean_vector{1, 2, 3};
		auto const b = comp6771::euclidean_vector{1, 2, 3};

		CHECK(abs(dot(a, b) - 14) < 0.0001);
	}

	SECTION("Non-Const Non-Const Dot Utility") {
		auto a = comp6771::euclidean_vector{1, 2, 3};
		auto b = comp6771::euclidean_vector{1, 2, 3};

		CHECK(abs(dot(a, b) - 14) < 0.0001);
	}

	SECTION("Const Non-Const Dot Utility") {
		auto const a = comp6771::euclidean_vector{1, 2, 3};
		auto b = comp6771::euclidean_vector{1, 2, 3};

		CHECK(abs(dot(a, b) - 14) < 0.0001);
	}

	SECTION("Mismatching Dimensions Dot Utility") {
		auto a = comp6771::euclidean_vector{1, 2, 3};
		auto b = comp6771::euclidean_vector{4};

		CHECK_THROWS(dot(a, b));
	}

	SECTION("LHS Empty Dot Utility") {
		auto v = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector{1, 2, 3};

		CHECK_THROWS(dot(a, b));
	}

	SECTION("RHS Empty Dot Utility") {
		auto v = std::vector<double>{};
		auto a = comp6771::euclidean_vector{1, 2, 3};
		auto b = comp6771::euclidean_vector(v.begin(), v.end());

		CHECK_THROWS(dot(a, b));
	}

	SECTION("LHS and RHS Empty Dot Utility") {
		auto v = std::vector<double>{};
		auto w = std::vector<double>{};
		auto a = comp6771::euclidean_vector(v.begin(), v.end());
		auto b = comp6771::euclidean_vector(w.begin(), w.end());

		CHECK(abs(dot(a, b)) < 0.0001);
	}
}
